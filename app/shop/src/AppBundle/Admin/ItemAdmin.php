<?php

namespace AppBundle\Admin;

use AppBundle\Entity\Category;
use AppBundle\Entity\Item;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Class ItemAdmin
 * @package AppBundle\Admin
 */
class ItemAdmin extends AbstractAdmin
{
    /**
     * @param $object
     * @return string
     */
    public function toString($object)
    {
        return $object instanceof Item
            ? $object->getName()
            : 'Item'; // shown in the breadcrumb on the create view
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Item name ang slug', ['class' => 'col-md-6'])
                ->add('name', TextType::class)
                ->add('slug', TextType::class)
            ->end()
            ->with('ItemCategories', ['class' => 'col-md-6'])
                ->add('categories',
                    ModelType::class, [
                    'class' => Category::class,
                    'property' => 'name',
                    'multiple' => true,
                ])
            ->end()
        ;
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
        $datagridMapper->add('slug');
        $datagridMapper
            ->add('categories', null, [], EntityType::class, [
                'class'    => Category::class,
                'choice_label' => 'name',
            ])
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id');
        $listMapper->addIdentifier('name');
        $listMapper->addIdentifier('slug');
        $listMapper->addIdentifier('categories', null, array(
            'route' => array(
                'name' => 'edit'
            )
        ))
        ;
    }
}
