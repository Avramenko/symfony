<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ItemController extends Controller
{
    /**
     *  Matches /item/*
     *
     * @Route("/item/{slug}", name="item_show")
     * @param $slug
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction($slug)
    {
        return $this->render('item/show.html.twig', array(
            'slug' => $slug,
        ));
    }
}