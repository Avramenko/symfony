<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CategoryController extends Controller
{
    /**
     *  Matches /category/*
     *
     * @Route("/category/{slug}", name="category_show")
     * @param $slug
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction($slug)
    {
        return $this->render('category/show.html.twig', array(
            'slug' => $slug,
        ));
    }
}