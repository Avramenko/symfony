Symfony 2.8
===

Install project

    git clone https://gitlab.com/Avramenko/symfony.git ~/apps/simfony
    cd ~/apps/symfony
    chmod +x setup update ubuntu-docker-install.sh
    ./setup
    symfony start composer-install restart
    symfony -s php -c "cd /app/shop/app && php ./console cache:clear"
    symfony -s php -c "cd /app/shop/app && php ./console assets:install"
    symfony -s php -c "cd /app/shop/app && php ./console doctrine:generate:entity"
    symfony restart

To install Docker on Ubuntu use:

    cd ~/apps/symfony && ./ubuntu-docker-install.sh
    
