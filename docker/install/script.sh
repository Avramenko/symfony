#!/usr/bin/env bash
#clear
set -e
APP_ENV=
PROJECT_PWD=

# подтягиваем переменные
. "${PROJECT_PWD}"/.env

# config variables
DATE=$(date +%F\ %T)
VERBOSE=false
DEBUG=false
HELP=false
SAVE_LOG=
INDEX_LIST=()
APPLICATION_LIST=()
# all services
# SERVICE_LIST=(`docker-compose -f common.yml -f "${APP_ENV}".yml config --services`)
SERVICE_LIST=()
PROCESS_LIST=()
PROVIDER_LIST=()
COMMAND=
WITH_CACHE=
DUMP_NAME=

#docker variables
INTERACTIVE="-i"
TTY="-t"
ADMINER=""
NETWORK_NAME=""

_showed_traceback=f

traceback() {
	# Hide the traceback() call.
	local -i start=$(( ${1:-0} + 1 ))
	local -i end=${#BASH_SOURCE[@]}
	local -i i=0
	local -i j=0

    if [[ ${VERBOSE} == true ]]; then
        echo "Traceback (last called is first):" 1>&2
    fi

	for ((i=${start}; i < ${end}; i++)); do
		j=$(( $i - 1 ))
		local function="${FUNCNAME[$i]}"
		local file="${BASH_SOURCE[$i]}"
		local line="${BASH_LINENO[$j]}"
		if [[ ${VERBOSE} == true ]]; then
            echo "     ${function}() in ${file}:${line}" 1>&2
        fi
	done
}

on_error() {
    local _ec="$?"
    local _cmd="${BASH_COMMAND:-unknown}"
    traceback 1
    _showed_traceback=t
    if [[ ${VERBOSE} == true ]]; then
        echo "The command ${_cmd} exited with exit code ${_ec}." 1>&2
    fi

}
trap on_error ERR


on_exit() {
    if [[ ${VERBOSE} == true ]]; then
        echo "Cleaning up before Exit ..."
    fi
    local _ec="$?"
    if [[ $_ec != 0 && "${_showed_traceback}" != t ]]; then
        traceback 1
    fi
}
trap on_exit EXIT


usage() {
    echo "usage: "${PROJECT_NAME}" -k value --key value --key=value -- command command command"
#    echo "      --log                               save all STDOUT to "${LOG_FILE}""
    echo
    echo "      install                             Команда для инсталяции проекта в окружении ${APP_ENV}"
    echo
    echo "      start                               Запуск проекта или сервиса"
    echo "                                          ${PROJECT_NAME} start"
    echo "              -s | --service              --service=name | -s name to build"
    echo "                                          ${PROJECT_NAME} --service=php start"
    echo "                                          ${PROJECT_NAME} -s php start"
    echo "      stop                                Остановка проекта или сервиса"
    echo "                                          ${PROJECT_NAME} stop"
    echo "              -s | --service              --service=name | -s name to stop"
    echo "                                          ${PROJECT_NAME} --service=node stop"
    echo "                                          ${PROJECT_NAME} -s node -s php stop"
    echo "      restart                             Перезапуск проекта или сервиса"
    echo "                                          ${PROJECT_NAME} restart"
    echo "              -s | --service              --service=name | -s name restart"
    echo "                                          ${PROJECT_NAME} --service=node restart"
    echo "                                          ${PROJECT_NAME} -s node -s php restart"
    echo
    echo "      ps                                  список состояние запущенных контейнеров"
    echo
    echo "      check                               проверка конфигурации окружения ${APP_ENV}"
    echo
    echo "      logs                                логи всех сервисов"
    echo "              -s | --service              --service=name | -s name to show logs"
    echo "                                          Список логов конкретных сервисов или сервиса"
    echo "                                          ${PROJECT_NAME} --service=node logs"
    echo "                                          ${PROJECT_NAME} -s node -s php logs"
    echo
    echo "      build                               build всех images"
    echo "              -n | --no-cache             build всех images или набора image без кэша"
    echo "              -s | --service              ${PROJECT_NAME} --service=php build"
    echo "                                          ${PROJECT_NAME} -s php -s db build"
    echo
    echo "      restore                             восстановление бэкапа БД для всех сервисов"
    echo "              -s | --service              восстановление бэкапа БД для --service=name | -s name"
    echo
    echo "      backup                              создание бэкапов для всех баз"
    echo "              -s | --service              создание бэкапа для --service=service_name или -s service_name"
    echo
    echo "      migrate                             запускает миграции для всех БД"
    echo "              -s | --service              миграция БД для --service=name | -s name"
    echo
    echo "      execute                             выполниьт команду в сервисе"
    echo "              -s | --service              --service=name for execute command"
    echo "              -c | --command              --command=\"command to run in service container\""
    echo
    echo "      composer-install                    composer install --prefer-dist"
    echo
    echo "      init                                php init for yii2 application"
    echo
    echo "      update                              update all applications yii and react"
    echo "              -a | --app                  ${PROJECT_NAME} --app=yii --app=react update или с короткими ключами"
    echo "                                          ${PROJECT_NAME} -a yii -a react update"
    echo
    echo "      upgrade                             обновление yii и react"
    echo
    echo "      full-upgrade                        full upgrade project with yarn, yarn-build and recreate-index commands"
    echo
    echo "      pull                                git pull for current project brunch"
    echo
    echo "      services                            список всех доступных сервисов"
    echo
    echo "      volumes                             список всех виртуальных дисков"
    echo
    echo "Optional arguments:"
    echo
    echo "      -v | --verbose                      run script in VERBOSE mode, print interactive script messages to STOUT"
    echo "      -d | --debug                        run script in DEBUG mode, print all script process to STDOUT"
    echo
    echo "      -h | --help                         display this help and exit"
    echo
    echo "
Примеры комадн:

1) Установка приложения после клонирования и выполнения ./setup

    ${PROJECT_NAME} create

2) Запуск приложения:

    ${PROJECT_NAME} start

3) Остановка приложения:

    ${PROJECT_NAME} stop

4) Перезапуск приложения, рестарт (остановка и запуск)

    ${PROJECT_NAME} restart

6) Инициализация приложения (php init)

    ${PROJECT_NAME} init

7) Обновление приложений symfony

    ${PROJECT_NAME} update

8) Обновление yii и react, выгрузка данных из Market и ABC

    ${PROJECT_NAME} update upload reindex

9)  Команды можно перечислить через пробел, команды выполняются в указанном порядке

    ${PROJECT_NAME} check build start composer-install init restore migrate

10) Пример восстановления БД ${PROJECT_NAME}, пример использования длинных и коротких ключей:

    ${PROJECT_NAME} --service=db restore
    ${PROJECT_NAME} -s db restore

11) Пример создания новой миграции, пример использования длинных и коротких ключей:

    ${PROJECT_NAME} --service=php --command=\"php yii migrate/create my_new_migration_name\" execute
    ${PROJECT_NAME} -s php -c \"php yii migrate/create my_new_migration_name\" execute

12) Пример сборки docker образов:

    ${PROJECT_NAME} build                            - сборка всех образов docker используя кэш
    ${PROJECT_NAME} -n build                         - сборка образов docker без кэш
    ${PROJECT_NAME} -s php build                     - сборка php образа используя кэш
    ${PROJECT_NAME} -n -s php build                  - сборка php образа без кэш
    ${PROJECT_NAME} -n -s php -s db build            - сборка php и db образов без использвания кэша

13) Ключи не принимают значения:

    -h | --help        - отобразить эту справку
    -v | --verbose     - отображать в консоле подробную информацию
    -d | --debug       - запуск команд в DEBUG режиме
    -n | --no-cache    - использовать cache при сборке docker образов

14) Список с обязательным присвоением значений

    -s | --service     - название сервиса
    -c | --command     - команда для выполенения в конкретном сервисе
    -a | --app         - незвание приложения

15) Значения длинным ключам присваиваются с помощью знака равенства, например:

    ${PROJECT_NAME} --service=php --app=symfony --index=living-apartment

16) Значения коротких ключей присваиваются через пробел, например:

    ${PROJECT_NAME} -s php -a symfony update

17) Все команды указываются в конце после указания всех ключей:

    ${PROJECT_NAME} -k value --key value -- command command command

"
}

OPTS=`getopt -o vdhs:c:a:i:np: --long verbose,debug,help,service:,command:,app:,index:,no-cache,dump-name:,provider:,no-tty,no-interactive,network -- "$@"`

if [ $? != 0 ] ; then echo "Failed parsing options." >&2 ; exit 1 ; fi

#echo "$OPTS"
eval set -- "${OPTS}"

while true; do
  case "$1" in
    -v | --verbose )
        set -v
        VERBOSE=true
        shift
    ;;
    -d | --debug )
        set -x
        DEBUG=true
        shift
    ;;
    -h | --help )
        HELP=true
        shift
    ;;
    --no-interactive )
        INTERACTIVE=
        shift
    ;;
    --no-tty )
        TTY=
        shift
    ;;
    -s | --service )
        case "$2" in
            "")
                shift 2
            ;;
            *)
                SERVICE_LIST=("${SERVICE_LIST[@]}" "$2")
                shift 2
            ;;
        esac
    ;;
    -c | --command )
        case "$2" in
            "")
                shift 2
            ;;
            *)
                COMMAND="${2}"
                shift 2
            ;;
        esac
    ;;
    -n | --no-cache )
        WITH_CACHE="--no-cache"
        shift
    ;;
    --network )
        NETWORK_NAME="${PROJECT_NAME}"
        shift
    ;;
    -a | --app )
        case "$2" in
            "")
                shift 2
            ;;
            *)
                APPLICATION_LIST=("${APPLICATION_LIST[@]}" "$2")
                shift 2
            ;;
        esac
    ;;
    -i | --index )
        case "$2" in
            "")
                shift 2
            ;;
            *)
                INDEX_LIST=("${INDEX_LIST[@]}" "$2")
                shift 2
            ;;
        esac
    ;;
    -p | --provider )
        case "$2" in
            "")
                shift 2
            ;;
            *)
                PROVIDER_LIST=("${PROVIDER_LIST[@]}" "$2")
                shift 2
            ;;
        esac
    ;;
    --dump-name )
        case "$2" in
            "")
                shift 2
            ;;
            *)
                DUMP_NAME="$2"
                shift 2
            ;;
        esac
    ;;
    -- )
        shift
        #echo "Создаем PROCESS_LIST"
        PROCESS_LIST=("$@")
        #echo "попали сюда ===================="
        #echo "PROCESS_LIST="${PROCESS_LIST[@]}""
        #echo "Закончили....."
        break
    ;;
    * )
       break
    ;;
  esac
done

# Список всех сервисов
services()
{
    cd "${PROJECT_PWD}"
    docker-compose -f common.yml -f "${APP_ENV}".yml config --services
}

# Список всех виртуальных дисков
volumes()
{
    cd "${PROJECT_PWD}"
    docker-compose -f common.yml -f "${APP_ENV}".yml config --volumes
}

# проверка конфигурации
check()
{
    cd "${PROJECT_PWD}"
    docker-compose -f common.yml -f "${APP_ENV}".yml config
}

# команда для билда всех контейнеров
build()
{
    cd "${PROJECT_PWD}"
    if [[ "${#SERVICE_LIST[@]}" > 0 ]]; then
        docker-compose -f common.yml -f "${APP_ENV}".yml build ${WITH_CACHE} ${SERVICE_LIST[@]}
    else
        docker-compose -f common.yml -f "${APP_ENV}".yml build ${WITH_CACHE}
    fi
}
# Старт приложения включая проксю
start()
{
    network
    if [[ "${#APPLICATION_LIST[@]}" > 0 ]]; then
        for app in "${APPLICATION_LIST[@]}"
        do
            case "${app}" in
            "portainer" )
                cd "${PROJECT_PWD}"
                docker-compose -f portainer.yml up -d
            ;;
            esac
        done
    fi

    if [[ "${#SERVICE_LIST[@]}" > 0 ]]; then
        cd "${PROJECT_PWD}"
        docker-compose -f common.yml -f "${APP_ENV}".yml start ${SERVICE_LIST[@]}
    fi

    if [ "${#SERVICE_LIST[@]}" == 0 ] && [ "${#APPLICATION_LIST[@]}" == 0 ]; then
        cd "${PROJECT_PWD}"
        docker-compose -f common.yml -f "${APP_ENV}".yml up -d
    fi
}

# Остановка приложений и сервисов, сеть не трогаем
stop()
{
    if [[ "${#APPLICATION_LIST[@]}" > 0 ]]; then
        for app in "${APPLICATION_LIST[@]}"
        do
            case "${app}" in
            "portainer" )
                cd "${PROJECT_PWD}"
                docker-compose -f portainer.yml down
            ;;

            esac
        done
    fi

    if [[ "${#SERVICE_LIST[@]}" > 0 ]]; then
        cd "${PROJECT_PWD}"
        #docker-compose -f common.yml -f "${APP_ENV}".yml stop ${SERVICE_LIST[@]}
        yes | docker-compose -f common.yml -f "${APP_ENV}".yml rm --stop --force ${SERVICE_LIST[@]}
    fi

    if [ "${#SERVICE_LIST[@]}" == 0 ] && [ "${#APPLICATION_LIST[@]}" == 0 ]; then
        cd "${PROJECT_PWD}"
        docker-compose -f common.yml -f "${APP_ENV}".yml down
    fi

    if [[ "${NETWORK_NAME}" == "${PROJECT_NAME}" ]]; then
        docker network rm "${NETWORK_NAME}"
    fi
}
# рестарт приложения это остановка и включение
restart(){
    if [[ "${#APPLICATION_LIST[@]}" > 0 ]]; then
        for app in "${APPLICATION_LIST[@]}"
        do
            case "${app}" in
            "teamcity" )
                cd "${TEAMCITY_PWD}"
                docker-compose down
                docker-compose up -d
            ;;
            "agent" )
                cd "${TEAMCITY_PWD}"
                docker-compose -f agent.yml down
                docker-compose -f agent.yml up -d
            ;;
            esac
        done
    fi

    if [[ "${#SERVICE_LIST[@]}" > 0 ]]; then
        cd "${PROJECT_PWD}"
        for s in "${SERVICE_LIST[@]}"
        do
            #docker-compose -f common.yml -f "${APP_ENV}".yml stop "${s}"
            yes | docker-compose -f common.yml -f "${APP_ENV}".yml rm --stop --force "${s}"
            docker-compose -f common.yml -f "${APP_ENV}".yml create "${s}"
            docker-compose -f common.yml -f "${APP_ENV}".yml start "${s}"
        done
    fi

    if [ "${#SERVICE_LIST[@]}" == 0 ] && [ "${#APPLICATION_LIST[@]}" == 0 ]; then
        stop
        start
    fi
}

# проверка статуса запущенных контейнеров
ps()
{
    if [[ "${#APPLICATION_LIST[@]}" > 0 ]]; then
        for app in "${APPLICATION_LIST[@]}"
        do
            case "${app}" in
            "portainer" )
                cd "${PROJECT_PWD}"
                docker-compose -f portainer.yml ps
            ;;
            "all" )
                cd "${PROJECT_PWD}"
                docker-compose -f portainer.yml ps
                docker-compose -f common.yml -f "${APP_ENV}".yml ps
            ;;
            esac
        done
    else
        cd "${PROJECT_PWD}"
        docker-compose -f common.yml -f "${APP_ENV}".yml ps
    fi
}

# Просмотр логов всех контейнеров
logs(){
    cd "${PROJECT_PWD}"
    if [[ "${#SERVICE_LIST[@]}" > 0 ]]; then
        for i in "${SERVICE_LIST[@]}"
        do
            docker-compose -f common.yml -f "${APP_ENV}".yml logs "${i}"
        done
    else
        docker-compose -f common.yml -f "${APP_ENV}".yml logs
    fi

}

# команда для восстановления БД из Selectel
restore()
{
    cd "${PROJECT_PWD}"
    docker exec "${INTERACTIVE}" "${TTY}" "${PROJECT_NAME}"_"${APP_ENV}"_db /utils/restore.sh
}

# команда для backup БД
backup()
{
    cd "${PROJECT_PWD}"
    docker exec "${INTERACTIVE}" "${TTY}" "${PROJECT_NAME}"_"${APP_ENV}"_db /utils/backup.sh
}

# команда для получения списка бэкапов в Selectel
list()
{
    cd "${PROJECT_PWD}"
    docker exec "${INTERACTIVE}" "${TTY}" "${PROJECT_NAME}"_"${APP_ENV}"_db /utils/list.sh
}

execute()
{
    if [[ "${#SERVICE_LIST[@]}" > 0 ]];
    then
        for i in "${SERVICE_LIST[@]}"
        do
            docker exec "${INTERACTIVE}" "${TTY}" "${PROJECT_NAME}"_"${APP_ENV}"_"${i}" ${COMMAND}
        done
    fi
}

update()
{
    cd "${PROJECT_PWD}"
    composer-install
    init
    migrate
}

composer-install(){
    docker exec "${INTERACTIVE}" "${TTY}" "${PROJECT_NAME}"_"${APP_ENV}"_php composer install --prefer-dist --no-progress
}

init(){
    if [ "${APP_ENV}" == "prod" ];
    then
        docker exec "${INTERACTIVE}" "${TTY}" "${PROJECT_NAME}"_"${APP_ENV}"_php php init --env=Production --overwrite=y
    else
        docker exec "${INTERACTIVE}" "${TTY}" "${PROJECT_NAME}"_"${APP_ENV}"_php php init --env=Development --overwrite=y
    fi
}

migrate(){
    # TODO Исправить миграцию на отдельные сервисы БД, пока что одна db_living
    docker exec "${INTERACTIVE}" "${TTY}" "${PROJECT_NAME}"_"${APP_ENV}"_php php yii migrate --interactive=0
    docker exec "${INTERACTIVE}" "${TTY}" "${PROJECT_NAME}"_"${APP_ENV}"_php php yii db-maintenance/recreate-utils
}

# функция для инициализации проекта
install()
{
    check
    build
    start
    update
}

# Обновление проекта для разработчика, нужно выполнять как можно чаще в той ветке в которой работаеш
upgrade()
{
    start

    if [ "${APP_ENV}" == "prod" ]; then
        backup
    else
        restore
    fi
    update
    if [ "${APP_ENV}" == "prod" ]; then
        backup
    fi
}

full-upgrade()
{
    upgrade
}

pull()
{
    cd "${PROJECT_PWD}"
    git pull
}

# Определяем включена ли сеть для сервисов
# Network ID нужен для того что бы проверить доступ к сети, все сервисы работают внутри одной сети
network()
{
    NETWORK_ID=`docker network ls --filter name="${PROJECT_NAME}" -q`
    if [[ "${#NETWORK_ID}" == 0 ]]; then
        # Если сеть отсутствует для проекта то создаем её, сеть должна быть поднята всегда
        DOCKER_COMPOSE_VERSION=`docker-compose version --short`
        docker network create "${PROJECT_NAME}" \
            -d bridge \
            --scope local \
            --attachable \
            --label com.docker.compose.network="${PROJECT_NAME}" \
            --label com.docker.compose.project="${PROJECT_NAME}" \
            --label com.docker.compose.version="${DOCKER_COMPOSE_VERSION}"

        # Пример для запуска сети вручную для проекта living
        # docker network create living \
        #   -d bridge \
        #   --scope local \
        #   --attachable \
        #   --label com.docker.compose.network=living \
        #   --label com.docker.compose.project=living \
        #   --label com.docker.compose.version=1.21.2
        #
        # docker inspect -f '{{ .HostConfig.Ulimits }}' container_name
    fi
}

# запускаем команды поочереди, в том порядке в котором запросил пользователь
if [[ "${#PROCESS_LIST[@]}" > 0 ]]; then
    #echo "Команды запускаются по порядку как указано в команде запуска!"
    for i in "${PROCESS_LIST[@]}"
    do
        #echo "Выполняем команду: ${i}"
        "${i}"
    done
else
    usage
    exit 0
fi

exit