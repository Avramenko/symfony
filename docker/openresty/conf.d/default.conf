server {
    listen       80;
    server_name  static.symfony.local;

    #charset koi8-r;
    #access_log  /var/log/nginx/host.access.log  main;

    set $secret symfony;
    set $images_pwd /static;
    set $cache_pwd /cache;

    location @image_server {
      content_by_lua_file "/lua/serve_image.lua";
      lua_code_cache off;
    }

    location / {
       default_type text/html;
       content_by_lua 'ngx.say("<p>static.symfony.local</p>")';
    }

    #error_page  404              /404.html;

    # redirect server error pages to the static page /50x.html
    #
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /html;
    }

    location ~ ^/images/(?<sig>[^/]+)/(?<size>[^/]+)/(?<path>.*\.(?<ext>[a-zA-Z_]*))$ {
        root $cache_pwd;
        set_md5 $digest "$size/$path";
        try_files /$digest.$ext @image_server;
    }

    # proxy the PHP scripts to Apache listening on 127.0.0.1:80
    #
    #location ~ \.php$ {
    #    proxy_pass   http://127.0.0.1;
    #}

    # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
    #
    #location ~ \.php$ {
    #    root           /usr/local/openresty/nginx/html;
    #    fastcgi_pass   127.0.0.1:9000;
    #    fastcgi_index  index.php;
    #    fastcgi_param  SCRIPT_FILENAME  /scripts$fastcgi_script_name;
    #    include        fastcgi_params;
    #}

    # deny access to .htaccess files, if Apache's document root
    # concurs with nginx's one
    #
    location ~ /\.(ht|svn|git) {
           deny all;
    }
}